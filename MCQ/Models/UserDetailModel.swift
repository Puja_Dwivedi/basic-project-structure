//
//  FriendDetailModel.swift
//  AUX
//
//  Created by cis on 01/08/20.
//  Copyright © 2020 cis. All rights reserved.
//

import Foundation

// MARK: - UserDetailModel
struct UserDetailModel: Codable {
    let status: Bool?
    let message: String?
    let data: UserDetailData?
}

// MARK: - UserDetailData
struct UserDetailData: Codable {
    let userID: Int
    let userName, userFirstName, userLastName, userEmail, userImage, userDeviceID: String?
    let userDeviceToken, appleMusicID: String?
    let userActiveStatus: Int?
    let userCreateOn, userUpdateOn: String?
    let following, followers: Int?
    var isGuestLogin : Bool?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case userName = "user_name"
        case userEmail = "user_email"
        case userImage = "user_image"
        case userDeviceID = "user_device_id"
        case userDeviceToken = "user_device_token"
        case appleMusicID = "apple_music_id"
        case userActiveStatus = "user_active_status"
        case userCreateOn = "user_create_on"
        case userUpdateOn = "user_update_on"
        case userFirstName = "first_name"
        case userLastName = "last_name"
        case following, followers
        case isGuestLogin
    }
}
