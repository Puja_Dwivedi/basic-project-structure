//
//  Constants.swift
//  RideShare
//
//  Created by cis on 17/08/19.
//  Copyright © 2019 cis. All rights reserved.
//

import Foundation
import UIKit
struct Fonts {
    static let ProximaRegular16 = UIFont(name: FontName.ProximaNova.regular.description, size: 16)
    static let ProximaLight16 = UIFont(name: FontName.ProximaNova.light.description, size: 16)
    static let ProximaSemiBold16 = UIFont(name: FontName.ProximaNova.semiBold.description, size: 16)
    static let ProximaSemiBold24 = UIFont(name: FontName.ProximaNova.semiBold.description, size: 24)
    static let ProximaRegular14 = UIFont(name: FontName.ProximaNova.regular.description, size: 14)
    static let ProximaLight14 = UIFont(name: FontName.ProximaNova.light.description, size: 14)
    static let ProximaSemiBold14 = UIFont(name: FontName.ProximaNova.semiBold.description, size: 14)
}

public struct Constants {
    static let APP_NAME = "Safety Scope"
    static let NO_INTERNET = "No Internet Connection"
    static let SOMETHING_WENT_WRONG = "Something went wrong"
    static let ENTER_EMAIL = "Enter E-mail address"
    static let ENTER_VALID_EMAIL = "Enter Valid E-mail address"
    static let ENTER_PASSWORD = "Enter Password"
    static let ENTER_FULLNAME = "Enter your full name"
    static let ENTER_VALID_NUMBER = "Enter valid mobile number"
    static let TICK_CHECKBOX = "Please Accept terms and condition"
    static let ENTER_OLD_PASSWORD = "Enter your current password"
    static let ENTER_NEW_PASSWORD = "Enter new password"
    static let ENTER_CONFIRM_PASSWORD = "Enter confirm password"
    static let MATCH_CONFIRM_PASSWORD = "Confirm password should be same as new password"
    static let ENTER_VALID_COMPLAIN = "Enter valid complain or suggestions"
    static let ACCEPT_POCLICIES = "Please accept the policy to use safetyscope."
    static let AWS_ACCESSKEY = "AKIA4EKGWHESGTKDIZKL"
    static let AWS_SECRETKEY_ID = "VrQsMIneKaa/r8CDEmY+yFlMs2LlgKme8CHZoR0w"
    static let AWS_S3_BUCKETNAME = "safetyscope"
}

struct FontName {
    enum ProximaNova : CustomStringConvertible {
        
        case bold
        case regular
        case semiBold
        case light
        
        var description: String {
            switch self {
            case .bold:
                return "ProximaNova-Bold"
            case .semiBold:
                return "ProximaNova-Semibold"
            case .regular:
                return "ProximaNova-Regular"
            case .light:
                return "ProximaNova-Light"
            }
        }
    }
}

struct CommonModel : Codable {
    let status : Bool?
    let message : String?
    let data : JSONAny?
}

struct Colors {
    static let BorderColor  = UIColor(red:0.80, green:0.80, blue:0.80, alpha:1.0)
    static let WhiteColor   = UIColor.white
    static let BlackColor   = UIColor.black
    static let primaryBlue  = UIColor(red:0.00, green:0.67, blue:0.93, alpha:1.0)
    static let clearColor   = UIColor.clear
    static let greenColor   = UIColor(red:0.35, green:0.80, blue:0.31, alpha:1.0)
    static let GreyColor   = UIColor.gray
}

public extension Notification.Name {
    static let sessionStatusChanged = Notification.Name("SessionStatusChanged")
}

extension UserDefaults{
    enum keys {
        struct User {
            static let user_model = "user_model"
        }
    }
}
