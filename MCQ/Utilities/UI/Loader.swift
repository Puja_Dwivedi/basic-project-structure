//
//  Loader.swift
//  LaqueueCutomer
//
//  Created by Adityaraj Singh Gaharwar on 27/06/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
//import ACProgressHUD_Swift
import NVActivityIndicatorView
class Loader: NSObject {
    static let shared = Loader()
    var activityIndicator : NVActivityIndicatorView!
    var backgroundview : UIView!
    func showIndicator() {
        guard backgroundview == nil else {
            return
        }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let window = appDelegate.window
        let xAxis = (window?.center.x)!
        let yAxis = (window?.center.y)!
        backgroundview = UIView.init(frame: CGRect.init(x: 0.0, y: 0.0, width: window!.frame.size.width, height: window!.frame.size.height))
        backgroundview.backgroundColor = UIColor.black
        backgroundview.layer.opacity = 0.5
        
        let frame = CGRect(x: (xAxis - 40), y: (yAxis - 40), width: 80, height: 80)
        activityIndicator = NVActivityIndicatorView(frame: frame)
        activityIndicator.type = .circleStrokeSpin
        activityIndicator.color =  #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)// add your color
        
        backgroundview.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        window!.addSubview(backgroundview)
        
    }
    func showIndicarorTo(view : UIView){
        view.isUserInteractionEnabled = false
        backgroundview = UIView.init(frame: CGRect.init(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height))
        backgroundview.backgroundColor = #colorLiteral(red: 0.09411764706, green: 0.09411764706, blue: 0.09411764706, alpha: 1)
        
        activityIndicator = NVActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        activityIndicator.center = backgroundview.center
        activityIndicator.type = .semiCircleSpin
        activityIndicator.color = #colorLiteral(red: 0.9411254525, green: 0.8078811765, blue: 0.2627362609, alpha: 1)// add your color
        backgroundview.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        view.addSubview(backgroundview)
    }
    func hideIndicator() {
        //        ACProgressHUD.shared.hideHUD()
        if activityIndicator != nil {
            activityIndicator.stopAnimating()
            if backgroundview != nil {
                if backgroundview.superview != nil {
                    backgroundview.superview!.isUserInteractionEnabled = true
                }
                backgroundview.removeFromSuperview()
                backgroundview = nil
                
            }
        }
    }
}
